﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PawCare.Shared.DTOs
{
    public class Order
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Name must be less than 50 characters.")]
        public string PetName { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerPhoneNumber { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public string? Services { get; set; }
        public bool Status { get; set; }
        public bool? IsBadTempered { get; set; }
        public double? Price { get; set; }
    }
}
