﻿namespace PawCare.Shared.DTOs
{
    public class Pet
    {
        public string Name { get; set; }
        public string OwnerFirstName { get; set; }
        public string OwnerLastName { get; set; }
        public string OwnerPhoneNumber { get; set; }
    }
}
