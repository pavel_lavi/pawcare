﻿using PawCare.Shared.DTOs;

namespace PawCare.Shared.StaticData
{
    public class stabs
    {
        private string[] OwnerNames = new string[] { "Hila", "Shahar", "Mark", "Yoni", "Boris" };
        private string[] PetNames = new string[] { "Ralf", "John", "Zoro", "Ray", "Jack" };

        public List<Pet> PetsStaticData { get; set; }
        public List<PetOwner> PetOwnersStaticData { get; set; }
        public List<Order> OrdersStaticData { get; set; }

        public stabs()
        {
            PetsStaticData = new List<Pet>();
            PetOwnersStaticData = new List<PetOwner>();
            OrdersStaticData = new List<Order>();

            for (int i = 0; i < 4; i++)
            {
                var firstName = OwnerNames[i];
                var lastName = "poop";
                var phoneNumber = "05433213244";
                var petName = PetNames[i];


                PetsStaticData.Add(new Pet()
                {
                    Name = petName,
                    OwnerFirstName = firstName,
                    OwnerLastName = lastName,
                    OwnerPhoneNumber = phoneNumber,
                });
                PetOwnersStaticData.Add(new PetOwner()
                {
                    FirstName = firstName,
                    LastName = lastName,
                    PhoneNumber = phoneNumber,
                    Email = "bla@poop.com",
                    Address = "poop st 17, Zigmanda"
                });
            }

            for (int i = 0; i < 4; i++)
            {
                var petData = PetsStaticData[new Random().Next(0, 4)];
                OrdersStaticData.Add(new Order()
                {
                    Id = i + 1,
                    PetName = petData.Name,
                    OwnerFirstName = petData.OwnerFirstName,
                    OwnerLastName = petData.OwnerLastName,
                    OwnerPhoneNumber = petData.OwnerPhoneNumber,
                    OrderDate = DateTime.Now,
                    CheckOut = DateTime.Now.AddDays(new Random().Next(11, 30)),
                    CheckIn = DateTime.Now.AddDays(new Random().Next(1, 10)),
                    IsBadTempered = new Random().Next() > (int.MaxValue / 2),
                    Price = new Random().Next(120, 1400)
                });

            }
            for (int i = 0; i < 14; i++)
            {
                var petData = PetsStaticData[new Random().Next(0, 4)];
                OrdersStaticData.Add(new Order()
                {
                    Id = i + 1,
                    PetName = petData.Name,
                    OwnerFirstName = petData.OwnerFirstName,
                    OwnerLastName = petData.OwnerLastName,
                    OwnerPhoneNumber = petData.OwnerPhoneNumber,
                    OrderDate = DateTime.Now,
                    CheckOut = DateTime.Now.AddDays(new Random().Next(-10, -1)),
                    CheckIn = DateTime.Now.AddDays(new Random().Next(-30, -11)),
                    IsBadTempered = new Random().Next() > (int.MaxValue / 2),
                    Price = new Random().Next(120, 1400)
                });
            }

        }
    }
}