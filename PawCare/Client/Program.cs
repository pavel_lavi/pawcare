﻿using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Syncfusion.Blazor;
using Microsoft.JSInterop;
using System.Globalization;
using PawCare.Client.Shared;
using PawCare.Client;


var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

#region Localization

// Register the Syncfusion locale service to customize the  SyncfusionBlazor component locale culture
builder.Services.AddSingleton(typeof(ISyncfusionStringLocalizer), typeof(SyncfusionLocalizer));
// MetaData Localier service
builder.Services.AddSingleton<MetaDataLocalizer>();


// Set the default culture of the application
var defaultCulture = new CultureInfo("en-US");
CultureInfo.DefaultThreadCurrentCulture = defaultCulture;
CultureInfo.DefaultThreadCurrentUICulture = defaultCulture;

// Get JS Runtime service 
var host = builder.Build();
var jsInterop = host.Services.GetRequiredService<IJSRuntime>();

// Get modified Rtl from culture switcher
var rtlResult = await jsInterop.InvokeAsync<string>("rtlInfo.get");
if (String.IsNullOrEmpty(rtlResult))
{
    rtlResult = "false";
}
builder.Services.AddSyncfusionBlazor(options =>
{
    options.EnableRtl = bool.Parse(rtlResult);
    options.IgnoreScriptIsolation = true;
});

// Get modified culture from culture switcher
var cultureResult = await jsInterop.InvokeAsync<string>("cultureInfo.get");

if (cultureResult is not null && SupportedLanguages.Cultures.Keys.Contains(cultureResult))
{
    // Set the culture from culture switcher
    var culture = new CultureInfo(cultureResult);
    CultureInfo.DefaultThreadCurrentCulture = culture;
    CultureInfo.DefaultThreadCurrentUICulture = culture;
}

#endregion


await builder.Build().RunAsync();