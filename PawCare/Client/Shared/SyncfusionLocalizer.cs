﻿using Syncfusion.Blazor;
using System.Resources;

public class SyncfusionLocalizer : ISyncfusionStringLocalizer
{
    // To get the locale key from mapped resources file
    public string GetText(string key)
    {
        return this.ResourceManager.GetString(key);
    }

    // To access the resource file and get the exact value for locale key

    public ResourceManager ResourceManager
    {
        get
        {
            // Replace the ApplicationNamespace with your application name.
            return PawCare.Client.Resources.SfResources.ResourceManager;
        }
    }
}