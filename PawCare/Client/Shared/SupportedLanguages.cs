﻿namespace PawCare.Client.Shared
{
    public static class SupportedLanguages
    {
        public static Dictionary<string, bool> Cultures = new Dictionary<string, bool>()
            {
                {"en-US", false },
                {"he", true }
            };
    }
}
